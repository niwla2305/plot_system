minetest.register_node("plot_system:way_protect", {
    description = "Way Protector (if you have this node, please report to an admin)",
    drawtype = "airlike",
    paramtype = "light",
    groups = {not_in_creative_inventory = 1},
    pointable = false,
    diggable = true,
    walkable = false,
    sunlight_propagates = true,
    on_blast = function() end
})

local path_node_def = table.copy(minetest.registered_nodes[plot_system.path_node])
path_node_def.diggable = false
path_node_def.groups = {}
path_node_def.description = "Way Node (if you have this node, please report to an admin)"
path_node_def.on_blast = function() end
minetest.register_node("plot_system:path_node", path_node_def)

local bottom_node_def = table.copy(minetest.registered_nodes[plot_system.bottom_node])
bottom_node_def.diggable = false
bottom_node_def.groups = {}
bottom_node_def.description = "Bottom Node (if you have this node, please report to an admin)"
bottom_node_def.on_blast = function() end
minetest.register_node("plot_system:bottom_node", bottom_node_def)

plot_depth_pos = plot_system.plotposy - plot_system.plot_depth
local path_node_protect_id =  minetest.get_content_id("plot_system:way_protect")
local path_node_id = minetest.get_content_id("plot_system:path_node")
local bottom_node_id = minetest.get_content_id("plot_system:bottom_node")
local fill_node_id = minetest.get_content_id(plot_system.fill_node)
local cover_node_id = minetest.get_content_id(plot_system.cover_node)

minetest.register_node("plot_system:top_node", {
    description = "MyAir (you hacker you!)",
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,

    walkable     = true, -- Would make the player collide with the air node
    pointable    = false, -- You can't select the node
    diggable     = false, -- You can't dig the node
    buildable_to = false,  -- Nodes can be replace this node.
                        -- (you can place a node and remove the air node
                        -- that used to be there)

    drop = "",
    groups = {not_in_creative_inventory=0},
    on_blast = function() end
})
local top_node_id = minetest.get_content_id("plot_system:top_node")


minetest.register_on_generated(function(minp, maxp, blockseed)
    local vm = minetest.get_voxel_manip()
    local c1, c2 = vm:read_from_map(minp, maxp)
    local area = VoxelArea:new{MinEdge=c1, MaxEdge=c2}
    local data = vm:get_data()
    -- Im Grunde offset (c1) + blockpos, dann regelmäßige steps => noch Raum für Optimierung
    for i, _ in pairs(data) do
		local pos = area:position(i)
        local plot_size2 = plot_system.plot_size + plot_system.path_size


        if pos.y > plot_depth_pos and pos.y <= plot_system.plotposy then
            data[i] = fill_node_id
        end
        if pos.y == plot_system.plotposy then
            data[i] = cover_node_id
        end
        if pos.y == plot_depth_pos then
            data[i] = bottom_node_id
        end
        if pos.y == plot_system.plotposy + plot_system.plot_height +1 then
            data[i] = top_node_id
        end

        if pos.x % plot_size2 < plot_system.path_size or pos.z % plot_size2 < plot_system.path_size then
            if pos.y > plot_depth_pos and pos.y <= plot_system.plotposy then
                data[i] = path_node_id
            end
            if pos.y > plot_system.plotposy and pos.y <= plot_system.plotposy + plot_system.plot_height then
                data[i] = path_node_protect_id
            end
        end
	end

    vm:set_data(data)
    vm:calc_lighting()
    vm:update_liquids()
    vm:write_to_map()
end)

