
# Plot System
[Minetest](https://www.minetest.net/ "Link to minetest.net") mod adding a minecraft like plot system

## Dependencies
* default
* stairs
* lib_chatcmdbuilder

## Version
development -- no release yet

## License
CC BY-NC 3.0 | See [LICENSE](https://github.com/TalkLounge/plot_system/blob/master/LICENSE.md "Link to LICENSE.md")
Will be changed in the future

## Credits
**LMD**
Helping

**Niwla**
Will try to work on this


**Other contributors**  
See: [Other contributors](https://gitlab.com/Niwla23/plot_system/-/graphs/master "Link to other contributors")

## ToDo
* implement features like claiming, clearing etc.

