--plot_system global is defined in init.lua
function plot_system.claim(player)
    local playername = player:get_player_name()
    minetest.chat_send_player(playername, "test")
end

function plot_system.gotoplot(player, id)
    local playername = player:get_player_name()
    minetest.chat_send_player(playername, "Home")
    if id then
        minetest.chat_send_player(playername, id)
    end

end

function plot_system.random(player, id)
    local playername = player:get_player_name()
    minetest.chat_send_player(playername, "Home")
    player:set_pos({x=math.random(-31000, 31000), y=plot_system.plotposy, z=math.random(-31000, 31000)})

end