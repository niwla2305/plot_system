--[[
          @               @@
          @               @@
          @               @@
          @               @@
          @               @@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          @               @@
          @               @@
          @               @@
          @  plot_system  @@
          @               @@
          @               @@
          @               @@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          @               @@
          @               @@
          @               @@
          @               @@
          @               @@
--]]

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
plot_system = {}

plot_system.plot_size = 32
plot_system.path_node = "default:wood"
plot_system.path_size = 5
plot_system.plotposy = 10000
plot_system.plot_depth = 100
plot_system.plot_height = 100
plot_system.bottom_node = "default:cloud"plot_system. fill_node = "default:dirt"
plot_system.cover_node = "default:dirt_with_grass"


dofile(modpath.."/api.lua")
dofile(modpath.."/mapgen.lua")
dofile(modpath.."/commands.lua")