ChatCmdBuilder.new("p", function(cmd)
	cmd:sub("claim", function(name)
		local player = minetest.get_player_by_name(name)
        plot_system.claim(player)
	end)

	cmd:sub("home :id:int", function(name, id)
		local player = minetest.get_player_by_name(name)
		plot_system.gotoplot(player, id)
	end)

	cmd:sub("random", function(name)
		local player = minetest.get_player_by_name(name)
		plot_system.random(player)
    end)
    --add more subcommands here
end,
{
	description = "Commands for the plot_system",
    privs =
    {
		interact = true,
	}
})